//
//  HomeViewController.h
//  SimpleObjDemo
//
//  Created by pimpaporn chaichompoo on 10/10/16.
//  Copyright © 2016 Pimpaporn Chaichompoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MovieViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>
{
    NSMutableArray *title_array;
    NSMutableArray *detail_array;
    NSMutableArray *imdb_array;

    NSMutableArray *img_array;
    NSMutableArray *banner_array;
}

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *btn_movie;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btn_setting;

@end
