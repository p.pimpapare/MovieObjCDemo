//
//  Movie_CollectionViewCell.h
//  SimpleObjDemo
//
//  Created by pimpaporn chaichompoo on 10/10/16.
//  Copyright © 2016 Pimpaporn Chaichompoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Movie_CollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *sub_title;

-(void)set_cell:(NSString *)title_text img:(NSString *)img_text;

@end
