//
//  AppDelegate.h
//  MovieObjCDemo
//
//  Created by pimpaporn chaichompoo on 10/11/16.
//  Copyright © 2016 Pimpaporn Chaichompoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

