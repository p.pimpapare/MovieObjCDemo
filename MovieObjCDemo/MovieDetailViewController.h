//
//  MovieDetailViewController.h
//  SimpleObjDemo
//
//  Created by pimpaporn chaichompoo on 10/10/16.
//  Copyright © 2016 Pimpaporn Chaichompoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MovieDetailViewController : UIViewController

@property (strong,nonatomic) NSString *name_text;
@property (strong,nonatomic) NSString *detail_text;
@property (strong,nonatomic) NSString *imdb_text;
@property (strong,nonatomic) NSString *image_text;

@property (weak, nonatomic) IBOutlet UIImageView *m_image;
@property (weak, nonatomic) IBOutlet UILabel *m_name;
@property (weak, nonatomic) IBOutlet UITextView *m_detail;
@property (weak, nonatomic) IBOutlet UILabel *m_score;


@end
