//
//  Movie_CollectionViewCell.m
//  SimpleObjDemo
//
//  Created by pimpaporn chaichompoo on 10/10/16.
//  Copyright © 2016 Pimpaporn Chaichompoo. All rights reserved.
//

#import "Movie_CollectionViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation Movie_CollectionViewCell

-(void)set_cell:(NSString *)title_text img:(NSString *)img_text
{
    [self.title setText:[NSString stringWithFormat:@"%@",title_text]];

    [self.img sd_setImageWithURL:[NSURL URLWithString:img_text]
                placeholderImage:[UIImage imageNamed:@"bg_placeholder"]options:SDWebImageProgressiveDownload];
}

@end
