

// MovieObjDemo https://gitlab.com/p.pimpapare/MovieObjCDemo.git
// Export app icon https://makeappicon.com/

/*
- Create Cocoapod ( $ = Terminal)

$cd /Project path/

$pod init // Create pod file
$pod install // Install the dependencies in your project

open (ProjectName).xcworkspace in project folder

podfile example

platform :ios, '8.0'

target 'ProjectName' do

pod 'KDCycleBannerView', '~> 1.1'  # https://cocoapods.org/?q=KDCycleBannerView
pod 'DLDownload', '~> 0.4'  # https://cocoapods.org/?q=dldownload
pod 'SDWebImage', '~> 3.8' # https://cocoapods.org/?q=SDWebImage

end

$pod update // Update Cocoapod

*/


