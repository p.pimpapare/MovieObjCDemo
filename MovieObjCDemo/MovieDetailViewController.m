//
//  MovieDetailViewController.m
//  SimpleObjDemo
//
//  Created by pimpaporn chaichompoo on 10/10/16.
//  Copyright © 2016 Pimpaporn Chaichompoo. All rights reserved.
//

#import "MovieDetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface MovieDetailViewController ()

@end

@implementation MovieDetailViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self set_movie_info:self.name_text detail:self.detail_text score:self.imdb_text image:self.image_text];
}

-(void)set_movie_info:(NSString *)name detail:(NSString *)detail score:(NSString *)score image:(NSString *)image{
    
    [self.m_name setText:[NSString stringWithFormat:@"%@",name]];
    [self.m_detail setText:[NSString stringWithFormat:@"%@",detail]];
    [self.m_score setText:[NSString stringWithFormat:@"IMDb %@",score]];
    
    [self.m_image sd_setImageWithURL:[NSURL URLWithString:image]
                placeholderImage:[UIImage imageNamed:@"bg_placeholder"]options:SDWebImageProgressiveDownload];
}

@end
