//
//  HomeViewController.m
//  SimpleObjDemo
//
//  Created by pimpaporn chaichompoo on 10/10/16.
//  Copyright © 2016 Pimpaporn Chaichompoo. All rights reserved.
//

#import "MovieViewController.h"
#import "Movie_CollectionViewCell.h"
#import <KDCycleBannerView/KDCycleBannerView.h>
#import <DLDownload/DLDownload.h>

#import "MovieDetailViewController.h"

@interface MovieViewController ()<KDCycleBannerViewDataource, KDCycleBannerViewDelegate>

@property (weak, nonatomic) IBOutlet KDCycleBannerView *cycleBannerViewTop;
@property (strong, nonatomic) KDCycleBannerView *cycleBannerViewBottom;

@end

@implementation MovieViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self set_initail_object];
}

-(void)set_initail_object{
    
    [self get_service];
    [self set_banner];
    
    [self.collectionView registerClass:[Movie_CollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"Movie_CollectionViewCell" bundle:nil]  forCellWithReuseIdentifier:@"Cell"];
}

-(void)get_service{
    
    title_array = [[NSMutableArray alloc]init];
    detail_array = [[NSMutableArray alloc]init];
    imdb_array = [[NSMutableArray alloc]init];
    
    img_array = [[NSMutableArray alloc]init];
    banner_array = [[NSMutableArray alloc]init];
    
    DLDownload *download = [[DLDownload alloc] init];
    download.url = [NSURL URLWithString:@"http://wisdomlanna.com/api/movie"];
    [download setMethod:DLDownloadMethodGET];
    
    download.callback =  ^(NSData *data, NSError *error) {
        
        if(!error) {
                        
            NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:data options:0 error:0];

            for (int i=0; i < [jsonData[@"results"] count] ; i++) {
                
                [title_array addObject:[NSString stringWithFormat:@"%@",jsonData[@"results"][i][@"name"]]];
                [detail_array addObject:[NSString stringWithFormat:@"%@",jsonData[@"results"][i][@"desc"]]];
                [imdb_array addObject:[NSString stringWithFormat:@"%@",jsonData[@"results"][i][@"imdb"]]];
                
                [img_array addObject:[NSString stringWithFormat:@"%@",jsonData[@"results"][i][@"images"][@"thumbnail"]]];
                [banner_array addObject:[NSString stringWithFormat:@"%@",jsonData[@"results"][i][@"images"][@"banner"]]];
            }
        }
        
        [self.cycleBannerViewTop reloadDataWithCompleteBlock:nil];
        [self.collectionView reloadData];
    };
    [download start];
}

//##### Add KDCycleDelegate
-(void)set_banner{
    
    self.cycleBannerViewBottom = [KDCycleBannerView new];
    self.cycleBannerViewBottom.frame = CGRectMake(0,0,self.cycleBannerViewBottom.frame.size.width,self.cycleBannerViewBottom.frame.size.height);
    self.cycleBannerViewBottom.datasource = self;
    self.cycleBannerViewBottom.delegate = self;
    self.cycleBannerViewBottom.continuous = YES;
    self.cycleBannerViewTop.autoPlayTimeInterval = 3;
    
    [self.view addSubview:_cycleBannerViewBottom];
}

- (NSArray *)numberOfKDCycleBannerView:(KDCycleBannerView *)bannerView {
    return banner_array;
}

- (UIViewContentMode)contentModeForImageIndex:(NSUInteger)index {
    return UIViewContentModeScaleToFill;
}

- (UIImage *)placeHolderImageOfZeroBannerView {
    return [UIImage imageNamed:@"bg_placeholder"];
}
//##### End of KDCycleDelegate


//##### Add CollectionViewDelegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [title_array count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    Movie_CollectionViewCell *cell=(Movie_CollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    if(cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"Movie_CollectionViewCell" owner:self options:nil][0];
    }
    
    [cell set_cell:[NSString stringWithFormat:@"%@",title_array[indexPath.row]] img:[NSString stringWithFormat:@"%@",img_array[indexPath.row]]];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake([UIScreen mainScreen].bounds.size.width/3,200);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    MovieDetailViewController *view_class = [self.storyboard instantiateViewControllerWithIdentifier:@"MovieDetailViewController"];
    
    [view_class setName_text:[NSString stringWithFormat:@"%@",title_array[indexPath.row]]];
    [view_class setDetail_text:[NSString stringWithFormat:@"%@",detail_array[indexPath.row]]];
    [view_class setImdb_text:[NSString stringWithFormat:@"%@",imdb_array[indexPath.row]]];
    [view_class setImage_text:[NSString stringWithFormat:@"%@",img_array[indexPath.row]]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController pushViewController:view_class animated:YES];
    });
}
//##### End of CollectionViewDelegate

@end
