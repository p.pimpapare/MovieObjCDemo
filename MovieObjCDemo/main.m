//
//  main.m
//  MovieObjCDemo
//
//  Created by pimpaporn chaichompoo on 10/11/16.
//  Copyright © 2016 Pimpaporn Chaichompoo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
